
public class BinaryTreeRightSideView {
	  public class TreeNode {
		      int val;
		      TreeNode left;
		      TreeNode right;
		      TreeNode(int x) { val = x; }
		  }
}
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> list = new ArrayList<Integer>();
        if(root ==null)
            return list;
        Queue<TreeNode> q = new LinkedList<TreeNode>();
        q.offer(root);
        int size = q.size();
        while(!q.isEmpty()){
            TreeNode node = q.poll();
            size--;
            if(node.left!=null)
                q.offer(node.left);
            if(node.right!=null)
                q.offer(node.right);
            if(size==0){
                list.add(node.val);
                size = q.size();
            }
        }
        return list;  
}
