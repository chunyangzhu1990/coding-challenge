public class ThirdMaximumNumber {
	public int thirdMax(int[] nums) {
		if (nums.length == 0)
			return 0;

		TreeSet<Integer> set = new TreeSet<Integer>();

		for (int i : nums) {
			set.add(i);
			if (set.size() > 3)
				set.pollFirst();
		}

		if (set.size() < 3)
			return set.pollLast();

		return set.pollFirst();
	}
}
