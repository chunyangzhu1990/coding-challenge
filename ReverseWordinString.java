
public class ReverseWordinString {
    public void reverseWords(char[] s) {
        
        reverse(s, 0 , s.length -1);
        int i,j = 0;
        int len = s.length;
        for(i = 0, j = 0; j<len;j++){
            if(s[j]==' '){
                reverse(s,i,j-1);
                i = j+1;
            }
        }
        
        reverse(s, i ,len-1);
    
        
    }
    
     public char[] reverse(char[] s, int start, int end){
         
        for(int i= start, j = end; i<j; i++, j--){
            char temp = s[i];
            s[i] = s[j];
            s[j] = temp;
        }
        return s;
    }
}
