
public class Subsets {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        res.add(new ArrayList<Integer>());
        
       for(int i:nums){
           List<List<Integer>> temp = new ArrayList<>();
           for(List<Integer> list: res){
               List<Integer> a  = new ArrayList<Integer>(list);
               a.add(i);
               temp.add(a);
           }
          res.addAll(temp);

       }
       return res;
    }
}
