
public class LowestCommonAncestorofBinaryTree {
	 public class TreeNode {
	     int val;
	      TreeNode left;
	     TreeNode right;
	     TreeNode(int x) { val = x; }
	  }

	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if(root == p || root == q || root == null){
           return root;
       }
       
       TreeNode left = lowestCommonAncestor(root.left, p, q);
       TreeNode right = lowestCommonAncestor(root.right, p, q);
       
       // common ancestor found
       if(left != null && right != null){
           return root;
       }
       
       // only left child found
       if(left != null){
           return root;
       }
       
       // only right child found
       if(right != null){
           return right;
       }
       //if (left == null && right == null)
       return null;
	}
}