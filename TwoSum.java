public class TwoSum {
	public int[] twoSum(int[] nums, int target) {
		int[] rst = new int[2];
		if (nums.length == 0 || nums == null)
			return rst;
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();

		for (int i = 0; i < nums.length; i++) {
			if (map.containsKey(target - nums[i])) {
				rst[1] = i;
				rst[0] = map.get(target - nums[i]);
			} else {
				map.put(nums[i], i);
			}
		}

		return rst;
	}
}
